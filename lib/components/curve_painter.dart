import 'package:flutter/material.dart';

class CurvePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = Colors.amber;
    paint.strokeWidth = 5;

    /// draw a line
    canvas.drawLine(
      Offset(0, size.height / 2),
      Offset(size.width, size.height / 2),
      paint,
    );

    /// draw a circle
    paint.color = Colors.blue;
    paint.style = PaintingStyle.stroke;
    canvas.drawCircle(
        Offset(size.width / 2, size.height / 2), size.width / 4, paint);

    /// draw a path
    paint.color = Colors.green;

    var path = Path();
    // moveTo() : move my pen to a specific coordinate (starting point)
    path.moveTo(size.width / 3, size.height * 1 / 7);
    path.lineTo(size.width / 2, size.height * 2.3 / 9);
    path.lineTo(size.width * 3 / 4, size.height * 1 / 9);

    paint.style = PaintingStyle.stroke;
    canvas.drawPath(path, paint);

    /// draw a closure path
    var shape = Path();

    shape.moveTo(size.width / 3, size.height * 3 / 4);
    shape.lineTo(size.width / 2, size.height * 5 / 6);
    shape.lineTo(size.width * 3 / 4, size.height * 4 / 6);

    paint.style = PaintingStyle.fill;
    canvas.drawPath(shape, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
