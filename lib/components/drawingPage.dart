import 'package:flutter/material.dart';

import 'curve_painter.dart';

class DrawingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Drawing Practice"),
      ),
      body: CustomPaint(
        painter: CurvePainter(),
        child: Center(
          child: Text("Practice"),
        ),
      ),
    );
  }
}
